#!/bin/bash

RESET="\[\033[0m\]"
RED="\[\033[0;31m\]"
GREEN="\[\033[1;32m\]"
CYAN="\[\033[0;36m\]"
PURPLE="\[\033[0;35m\]"

DOTFILES_DIR="$HOME/dotfiles"
source "$DOTFILES_DIR/env"
source "$DOTFILES_DIR/aliases"
source "$DOTFILES_DIR/configs"
source "$DOTFILES_DIR/functions"

export PS1="${RESET}\A ${PURPLE}\w ${CYAN}\$(git_prompt)${RESET}bash> "
