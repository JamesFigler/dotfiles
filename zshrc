#!/bin/zsh

DOTFILES_DIR="$HOME/dotfiles"
source "$DOTFILES_DIR/env"
source "$DOTFILES_DIR/aliases"
source "$DOTFILES_DIR/aliases_zsh"
source "$DOTFILES_DIR/configs"
source "$DOTFILES_DIR/functions"
source "$DOTFILES_DIR/zsh_keybinds"

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

setopt prompt_subst

command -v fzf >/dev/null 2>&1 && source <(fzf --zsh)

export VIRTUAL_ENV_DISABLE_PROMPT=1

export PS1='%T %F{magenta}%~%f %F{cyan}$(git_prompt)%f%F{green}$(python_virtual_env)%fzsh> '
