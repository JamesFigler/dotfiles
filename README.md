# Dotfiles

## Installing
1. Clone the project to your home directory.
1. Run the `install.sh` script to automatically source each `rc` file. Supported shells:
  - `bash`
  - `zsh`

## Uninstalling
Simply delete the `dotfiles` directory. To fully clean up, you will need to manually remove references to these source files in your shell's `rc` file.
